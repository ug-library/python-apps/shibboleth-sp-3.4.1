# shibboleth-sp-3.4.1

## Description
NOT CURRENTLY USED AS OF April 3, 2024. Could not get the shibboleth response to function correctly behind 2 proxies within a container. It may have worked if we tried port 443 from the load balancer to the container, but that was not an option, especially since we had an alternative solution via the web gateway.

This is the shibboleth-sp-3.4.1 source compiled into the U of G Libraries shibboleth & Nginx webserver containers. The source needed to be patched as the needed changes have not been rolled into the shibboleth core. This also removes the UG dependencies on 
hosted shibboleth versions.

## Installation
Within a container build:

```
ENV SHIBBOLETH_SP_VERSION 3.4.1
RUN curl -fSL https://gitlab.com/ug-library/python-apps/shibboleth-sp-3.4.1/-/archive/main/shibboleth-sp-$SHIBBOLETH_SP_VERSION-main.tar.gz -o shibboleth-sp-$SHIBBOLETH_SP_VERSION.tar.gz \
    && tar -zxC /usr/src -f shibboleth-sp-$SHIBBOLETH_SP_VERSION.tar.gz \
    && rm shibboleth-sp-$SHIBBOLETH_SP_VERSION.tar.gz

# Build and install a specific version of Shibboleth
# https://shibboleth.atlassian.net/wiki/spaces/SP3/pages/2065335293/Environment
ENV SHIBSP_PREFIX=/opt/shibboleth-sp
ENV SHIBSP_CONFIG=/opt/shibboleth-sp/etc/shibboleth/shibboleth2.xml
ENV SHIBSP_LOGGING=/opt/shibboleth-sp/etc/shibboleth/shibd.logger
ENV SHIBSP_LIBDIR=/opt/shibboleth-sp/lib
ENV SHIBD-LOG=/var/log/shibboleth
ENV SHIBSP_LOGDIR=/var/log/shibboleth
ENV SHIBSP_CFGDIR=/opt/shibboleth-sp/etc/shibboleth/
ENV SHIBSP_RUNDIR=/opt/app/run
ENV LD_LIBRARY_PATH=/opt/shibboleth-sp/lib:$LD_LIBRARY_PATH

WORKDIR /usr/src/shibboleth-sp-$SHIBBOLETH_SP_VERSION
RUN PKG_CONFIG_PATH=/opt/shibboleth-sp/lib/pkgconfig ./configure --with-log4shib=/opt/shibboleth-sp --with-fastcgi --enable-apache-24 \
    --prefix=/opt/shibboleth-sp --with-xerces=/opt/shibboleth-sp --with-xmltooling=/opt/shibboleth-sp \
    && make clean && make -j 4 && make install
```

## Usage
An example is auth.lib.uoguelph.ca.

The services behind that dns run within a container that runs behind the Institutions load balancer. Requests within that container are double proxied, one at the load balance, another at the VM endpoint running containers (Envoy).

Nginx is built from source using this shibboleth source and allows us to configure secure paths for federated authorization. The services at auth.lib run python applications, but multiple languages and applications could 
be supported.

## Roadmap
Stable, no changes planned.

## Project status
Based on the stable release of 3.4.1 with a patch to function within a container behind a load balancer. Not many adjustments expected.
